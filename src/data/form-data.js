export const COLOR_LIST = {
    gray: "#d9d9d9",
    blue: "#40a9ff",
    teal: "#36cfc9",
    green: "#73d13d",
    red: "#ff4d4f",
    yellow: "#ffec3d",
    orange: "#ffa940",
    pink: "#f759ab",
    purple: "#9254de",
  }

  export const CATEGORY_LIST = [
    "uncategorized",
    "people",
    "travel",
    "nature",
    "food",
    "animal",
    "fashion",
    "lifestyle",
    "sport",
    "photography",
    "design",
    "event",
  ]