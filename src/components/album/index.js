import React, { useState, useContext } from "react"
import { Link } from "gatsby"
import * as AlbumService from "../../services/album"
import { StorageContext } from "../../context/StorageProvider"
import { AuthContext } from "../../context/AuthProvider"
import { COLOR_LIST, CATEGORY_LIST } from "../../data/form-data"
import {
  Breadcrumb,
  Row,
  Col,
  Button,
  Typography,
  Drawer,
  Form,
  Input,
  Radio,
  Checkbox,
  Select,
  message,
  Card,
  Tooltip,
  Tag,
} from "antd"
import {
  PlusOutlined,
  FolderFilled,
  SettingOutlined,
  EyeOutlined,
  CompassOutlined,
  StarOutlined,
} from "@ant-design/icons"
import FolderPopover from "./folder-popover"
const { Option } = Select
const { Text } = Typography
const { Meta } = Card

const formLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
}

const Album = () => {
  const { id } = useContext(AuthContext)
  const { albums } = useContext(StorageContext)
  const [drawer, setDrawer] = useState({ visible: false, loading: false })

  const colors = Object.keys(COLOR_LIST).map((key, index) => (
    <Radio.Button
      key={index}
      value={key}
      style={{ background: COLOR_LIST[key] }}
    ></Radio.Button>
  ))

  const categories = CATEGORY_LIST.sort((a, b) => a.localeCompare(b)).map(
    name => (
      <Option key={name} value={name}>
        {name.charAt(0).toUpperCase() + name.slice(1)}
      </Option>
    )
  )

  const showDrawer = () => {
    setDrawer({ visible: true, loading: false })
  }

  const [form] = Form.useForm()

  const handleCancelForm = () => {
    form.resetFields()
    setDrawer({ visible: false, loading: false })
  }

  const handleSubmitForm = values => {
    // set loading drawer
    setDrawer({ ...drawer, loading: true })

    const album = {
      ...values,
      created: new Date().toUTCString(),
      updated: new Date().toUTCString(),
      removed: false,
      size: 0,
      photos: [],
    }

    // add a new record to album collection
    AlbumService.addAlbum(id, album)
      .then(doc => {
        if (doc.id) {
          // reset form after submitted successfully
          handleCancelForm()
          message.success("album was created successfully.")
        } else {
          message.error("Failed to create a new album. Please try again.")
        }
      })
      .catch(err => message.error("Network error."))
  }

  return (
    <>
      <div style={{ padding: "8px" }}>
        <Breadcrumb style={{ paddingBottom: "16px" }}>
          <Breadcrumb.Item>Albums</Breadcrumb.Item>
        </Breadcrumb>
        <Row gutter={[24, 24]}>
          <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 6 }}>
            <Button className="add-album-button" onClick={() => showDrawer()}>
              <PlusOutlined style={{ fontSize: `1.2rem` }} />
              <span>Create a new album</span>
            </Button>
          </Col>
          {albums.map(item => {
            let component = null
            if (!item.removed)
              component = (
                <Col
                  key={item.id}
                  xs={{ span: 12 }}
                  sm={{ span: 8 }}
                  md={{ span: 6 }}
                >
                  <Card
                  className="album-card"
                    // cover={
                    //   <img
                    //     alt="example"
                    //     height="150"
                    //     style={{objectFit: "cover"}}
                    //     src={item.photos.length > 0 ? item.photos[item.photos.length-1].src : "https://filestore.community.support.microsoft.com/api/images/72e3f188-79a1-465f-90ca-27262d769841"}
                    //   />
                    // }
                    actions={[
                      <Tooltip title="View album">
                        <Link to={`/app/album/${item.id}`}>
                          <EyeOutlined key="view" />
                        </Link>
                      </Tooltip>,
                      <FolderPopover userId={id} folder={item}>
                        <Tooltip title="Settings">
                          <SettingOutlined key="setting" />
                        </Tooltip>
                      </FolderPopover>,
                    ]}
                  >
                    <Meta
                      avatar={
                        <FolderFilled
                          className="album-thumbnail-icon"
                          style={{
                            fontSize: "2rem",
                            color: COLOR_LIST[item.color],
                          }}
                        />
                      }
                      title={item.title}
                      description={
                        <div>
                          {item.favorite ? (
                            <Tag color="gold">
                              <StarOutlined />
                            </Tag>
                          ) : null}
                          {item.isPrivate ? null : (
                            <Tag color="blue">
                              <CompassOutlined />
                            </Tag>
                          )}
                          <Tag color="purple">{item.category}</Tag>
                        </div>
                      }
                    />
                  </Card>
                </Col>
              )
            return component
          })}
        </Row>
      </div>
      <Drawer
        title="New album"
        width="40%"
        onClose={() => handleCancelForm()}
        visible={drawer.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          {...formLayout}
          form={form}
          onFinish={handleSubmitForm}
          initialValues={{
            color: "gray",
            isPrivate: true,
            category: "uncategorized",
          }}
          className="new-album-form"
        >
          <Form.Item
            label="Title"
            name="title"
            hasFeedback
            rules={[{ required: true, message: "" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Color" name="color">
            <Radio.Group buttonStyle="solid">{colors}</Radio.Group>
          </Form.Item>
          <Form.Item label="Sharing" name="isPrivate" valuePropName="checked">
            <Checkbox>
              Private album
              <div style={{ marginLeft: "24px", fontSize: ".7rem" }}>
                <Text type="secondary">
                  Uncheck to make this album public. Your photos inside this
                  album will be exploreable and can be viewed by other users.{" "}
                </Text>
              </div>
            </Checkbox>
          </Form.Item>
          <Form.Item label="Category" name="category">
            <Select
              showSearch
              placeholder="Select"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {categories}
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{ ...formLayout.wrapperCol, offset: 6 }}
            className="new-album-form-actions"
          >
            <Button htmlType="button" onClick={() => handleCancelForm()}>
              Cancel
            </Button>
            <Button type="primary" htmlType="submit" loading={drawer.loading}>
              Create album
            </Button>
          </Form.Item>
        </Form>
      </Drawer>
    </>
  )
}

export default Album
