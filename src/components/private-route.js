import React from "react"
import { navigate } from "gatsby"
import { isLoggedIn } from "../services/auth"

const PrivateRoute = ({ component: Component, location, dispatch, ...rest }) => {
  if (!isLoggedIn() && location.pathname !== `/`) {
    navigate("/")
    return null
  }
  return <Component {...rest} dispatch={dispatch}/>
}
export default PrivateRoute