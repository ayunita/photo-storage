import React from "react"
import styled, { keyframes } from "styled-components"
import PropTypes from "prop-types"
import LazyLoad from "react-lazyload"

const ImageWrapper = styled.div`
  position: relative;
`

const loadingAnimation = keyframes`
  0% {
    background-color: #fff;
  }
  50% {
    background-color: #ccc;
  }
  100% {
    background-color: #fff;
  }
`

const Placeholder = styled.div`
  width: calc(100% - 16px);
  height: calc(100% - 16px);
  position: absolute;
  bottom: 8px;
  left: 8px;
  animation: ${loadingAnimation} 1s infinite;
`

const StyledImage = styled.img`
  padding: 8px;
`

const LazyImage = ({ children, src, alt, width, height }) => {
  const refPlaceholder = React.useRef()

  const removePlaceholder = () => {
    refPlaceholder.current.remove()
  }

  return (
    <ImageWrapper>
      <Placeholder ref={refPlaceholder} />
      <LazyLoad>
        <StyledImage
          onLoad={removePlaceholder}
          onError={removePlaceholder}
          alt={alt}
          src={src}
          width={width}
          height={height}
        />
        {children}
      </LazyLoad>
    </ImageWrapper>
  )
}

LazyImage.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
}

export default LazyImage
