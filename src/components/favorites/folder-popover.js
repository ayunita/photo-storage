import React, { useState } from "react"
import * as AlbumService from "../../services/album"
import { COLOR_LIST, CATEGORY_LIST } from "../../data/form-data"

import { Popover, Button, message, Modal, Input, Select, Radio } from "antd"
const { Option } = Select

const FolderPopover = ({ children, userId, folder }) => {
  const [actionPopover, setActionPopover] = useState(false)
  const [colorPopover, setColorPopover] = useState(false)

  const [modal, setModal] = useState({
    visible: false,
    title: "",
    key: "",
    value: "",
    button: "",
    loading: false,
  })
  const { title, color, category, isPrivate, favorite } = folder

  const categories = CATEGORY_LIST.sort((a, b) => a.localeCompare(b)).map(
    name => (
      <Option key={name} value={name}>
        {name.charAt(0).toUpperCase() + name.slice(1)}
      </Option>
    )
  )

  const colors = Object.keys(COLOR_LIST).map((key, index) => (
    <Radio.Button
      key={index}
      value={key}
      style={{ background: COLOR_LIST[key] }}
    ></Radio.Button>
  ))

  const openModal = (title, button, key, value) => {
    setActionPopover(false)
    setModal({
      title: title,
      button: button,
      key: key,
      value: value,
      visible: true,
    })
  }

  const handleCancel = () => {
    setModal({ ...modal, visible: false, loading: false })
  }

  const renameAlbum = () => {
    setModal({ ...modal, loading: true })
    AlbumService.updateAlbumField(userId, folder.id, "title", modal.value)
      .then(res => {
        handleCancel()
        // message.success("Title was updated successfully.")
      })
      .catch(err => message.error("Network error."))
  }

  const changeColor = val => {
    AlbumService.updateAlbumField(userId, folder.id, "color", val)
      .then(res => {
        setColorPopover(false)
        setActionPopover(false)
        // message.success("Album color was updated successfully.")
      })
      .catch(err => message.error("Network error."))
  }

  const updateAccess = () => {
    AlbumService.updateAlbumField(userId, folder.id, "isPrivate", !isPrivate)
      .then(res => {
        setActionPopover(false)
        // message.success("Sharing level was updated successfully.")
      })
      .catch(err => message.error("Network error."))
  }

  const updateFavorite = () => {
    AlbumService.updateAlbumField(userId, folder.id, "favorite", !favorite)
      .then(res => {
        setActionPopover(false)
        // if (favorite) message.success("Album was removed from Favorites.")
        // else message.success("Album was added to Favorites.")
      })
      .catch(err => message.error("Network error."))
  }

  const changeCategory = val => {
    AlbumService.updateAlbumField(userId, folder.id, "category", val)
      .then(res => {
        setActionPopover(false)
        // message.success("Category was updated successfully.")
      })
      .catch(err => message.error("Network error."))
  }

  const removeAlbum = () => {
    AlbumService.updateAlbumField(userId, folder.id, "removed", true)
      .then(res => {
        setActionPopover(false)
        // message.success("Album was deleted. Check Trash Bin to restore.")
      })
      .catch(err => message.error("Network error."))
  }

  return (
    <>
      <Popover
        placement="right"
        trigger="click"
        visible={actionPopover}
        onVisibleChange={setActionPopover}
        content={
          <div className="folder-actions">
            <Button
              icon={
                <span role="img" aria-label="emoji-pencil">
                  ✏️
                </span>
              }
              onClick={() =>
                openModal("Rename Album", "Rename", "title", title)
              }
            >
              Rename
            </Button>
            <Popover
              content={
                <Radio.Group
                  className="color-group-popover"
                  value={color}
                  buttonStyle="solid"
                  onChange={e => changeColor(e.target.value)}
                >
                  {colors}
                </Radio.Group>
              }
              trigger="click"
              placement="right"
              visible={colorPopover}
              onVisibleChange={setColorPopover}
            >
              <Button
                icon={
                  <span role="img" aria-label="emoji-pallete">
                    🎨
                  </span>
                }
              >
                Change color
              </Button>
            </Popover>
            <Button
              icon={
                <span role="img" aria-label="emoji-compass">
                  🧭
                </span>
              }
              onClick={() => updateAccess()}
            >
              Make it {isPrivate ? "public" : "private"}
            </Button>
            <Button
              icon={
                <span role="img" aria-label="emoji-category">
                  🗂️
                </span>
              }
            >
              Category
              <Select
                showSearch
                style={{ width: "100px", marginLeft: "8px" }}
                size="small"
                placeholder="Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
                value={category}
                onChange={val => changeCategory(val)}
              >
                {categories}
              </Select>
            </Button>
            <Button
              icon={
                <span role="img" aria-label="emoji-star">
                  ⭐
                </span>
              }
              onClick={() => updateFavorite()}
            >
              {favorite ? "Remove from Favorites" : "Add to Favorites"}
            </Button>
            <Button
              icon={
                <span role="img" aria-label="emoji-trash">
                  🗑️
                </span>
              }
              onClick={() => removeAlbum()}
            >
              Delete
            </Button>
          </div>
        }
      >
        {children}
      </Popover>
      <Modal
        className="short-form-modal"
        title={modal.title}
        visible={modal.visible}
        onCancel={handleCancel}
        footer={[
          <Button
            key="submit"
            type="primary"
            loading={modal.loading}
            onClick={renameAlbum}
          >
            {modal.button}
          </Button>,
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        <Input
          value={modal.value}
          onChange={e => setModal({ ...modal, value: e.target.value })}
        />
      </Modal>
    </>
  )
}

export default FolderPopover
