import React, { useContext } from "react"
import { Link } from "gatsby"
import { StorageContext } from "../../context/StorageProvider"
import { AuthContext } from "../../context/AuthProvider"
import { COLOR_LIST } from "../../data/form-data"
import { Row, Col, Card, Tooltip, Tag, Breadcrumb } from "antd"
import {
  FolderFilled,
  SettingOutlined,
  EyeOutlined,
  CompassOutlined,
  StarOutlined,
} from "@ant-design/icons"
import FolderPopover from "./folder-popover"
const { Meta } = Card

const Favorites = () => {
  const { id } = useContext(AuthContext)
  const { albums } = useContext(StorageContext)

  return (
    <>
      <div style={{ padding: "8px" }}>
        <Breadcrumb style={{ paddingBottom: "16px" }}>
          <Breadcrumb.Item>Favorites</Breadcrumb.Item>
        </Breadcrumb>
        <Row gutter={[24, 24]}>
          {albums.map(item => {
            let component = null
            if (!item.removed && item.favorite)
              component = (
                <Col
                  key={item.id}
                  xs={{ span: 12 }}
                  sm={{ span: 8 }}
                  md={{ span: 6 }}
                >
                  <Card
                    className="album-card"
                    actions={[
                      <Tooltip title="View album">
                        <Link to={`/app/album/${item.id}`}>
                          <EyeOutlined key="view" />
                        </Link>
                      </Tooltip>,
                      <FolderPopover userId={id} folder={item}>
                        <Tooltip title="Settings">
                          <SettingOutlined key="setting" />
                        </Tooltip>
                      </FolderPopover>,
                    ]}
                  >
                    <Meta
                      avatar={
                        <FolderFilled
                          className="album-thumbnail-icon"
                          style={{
                            fontSize: "2rem",
                            color: COLOR_LIST[item.color],
                          }}
                        />
                      }
                      title={item.title}
                      description={
                        <div>
                          {item.favorite ? (
                            <Tag color="gold">
                              <StarOutlined />
                            </Tag>
                          ) : null}
                          {item.isPrivate ? null : (
                            <Tag color="blue">
                              <CompassOutlined />
                            </Tag>
                          )}
                          <Tag color="purple">{item.category}</Tag>
                        </div>
                      }
                    />
                  </Card>
                </Col>
              )
            return component
          })}
        </Row>
      </div>
    </>
  )
}

export default Favorites
