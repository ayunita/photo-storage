import React, { useState, useEffect } from "react"
import axios from "axios"
import fileDownload from "js-file-download"
import Gallery from "react-photo-gallery"
import { Breadcrumb, Avatar, Button, Tag } from "antd"
import { DownloadOutlined } from "@ant-design/icons"
import LazyImage from "../lazy-image"
import * as AlbumService from "../../services/album"
import * as UserService from "../../services/user"
import { CATEGORY_LIST } from "../../data/form-data"
const { CheckableTag } = Tag

const tagsData = [...CATEGORY_LIST].sort()

const imageRenderer = ({ index, photo }) => {
  const downloadPhoto = (url, filename) => {
    axios
      .get(`https://cors-anywhere.herokuapp.com/${url}`, {
        responseType: "blob",
      })
      .then(res => {
        fileDownload(res.data, filename)
      })
  }

  return (
    <div key={index} className="explore-photo">
      <LazyImage
        alt={photo.name}
        src={photo.src}
        width={photo.width}
        height={photo.height}
      >
        <div className="explore-photo-detail">
          <div className="photo-username">
            <Avatar size="small" style={{ background: photo.color }}>
              {photo.user.charAt(0).toUpperCase()}
            </Avatar>
            {photo.user}
          </div>
          <div className="photo-download">
            <Button
              type="default"
              shape="circle"
              icon={<DownloadOutlined />}
              size="small"
              onClick={() => {
                downloadPhoto(photo.src, `${photo.name}${photo.ext}`)
              }}
            />
          </div>
        </div>
      </LazyImage>
    </div>
  )
}

const Explore = () => {
  const [album, setAlbum] = useState({})
  const [selectedTags, setSelectedTags] = useState([...tagsData])
  const [loading, setLoading] = useState(false)
  const [photoList, setPhotoList] = useState([])

  useEffect(() => {
    let unmounted = false

    UserService.getAllUsers().then(snapshot =>
      snapshot.forEach(doc => {
        const userId = doc.id
        AlbumService.getAllSharedAlbums(userId).then(snapshot2 =>
          snapshot2.forEach(doc2 => {
            // get shared albums
            const album = {
              username: doc.data().name,
              ...doc2.data(),
            }
            if (!unmounted) {
              if (!album.removed) setAlbum(album)
            }
          })
        )
      })
    )
    return () => {
      unmounted = true
    }
  }, [])

  useEffect(() => {
    if (Object.keys(album).length > 0 && album.constructor === Object) {
      const x = [...album.photos].map(item => {
        return {
          user: album.username,
          category: album.category,
          name: item.name,
          width: item.width,
          height: item.height,
          src: item.src,
          ext: item.ext,
        }
      })
      setPhotoList(photoList => [...photoList, ...x])
    }
  }, [album])

  useEffect(() => {
    setLoading(false)
  }, [selectedTags])

  return (
    <div>
      <div style={{ padding: "8px" }}>
        <Breadcrumb style={{ paddingBottom: "8px" }}>
          <Breadcrumb.Item>Explore Shared Photos</Breadcrumb.Item>
        </Breadcrumb>
        {tagsData.map(tag => (
          <CheckableTag
            key={tag}
            className="category-tag"
            checked={selectedTags.indexOf(tag) > -1}
            onChange={checked => {
              const nextSelectedTags = checked
                ? [...selectedTags, tag]
                : selectedTags.filter(t => t !== tag)
              setSelectedTags(nextSelectedTags)
              setLoading(true)
            }}
          >
            {tag}
          </CheckableTag>
        ))}
      </div>
      {loading ? null : (
        <Gallery
          photos={photoList.filter(i => selectedTags.includes(i.category))}
          renderImage={imageRenderer}
        />
      )}
    </div>
  )
}

export default Explore
