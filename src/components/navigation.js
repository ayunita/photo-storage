import React, { useContext } from "react"
import { useLocation } from "@reach/router"
import { Link } from "gatsby"
import { Menu, Progress } from "antd"
import {
  PictureOutlined,
  DeleteOutlined,
  CompassOutlined,
  StarOutlined,
} from "@ant-design/icons"
import { AuthContext } from "../context/AuthProvider"
import { StorageContext } from "../context/StorageProvider"

const MENUITEMS = {
  album: {
    key: 1,
    name: "Albums",
    link: "/app/album",
    icon: <PictureOutlined />,
  },
  fav: {
    key: 2,
    name: "Favorites",
    link: "/app/fav",
    icon: <StarOutlined />,
  },
  trash: {
    key: 3,
    name: "Trash Bin",
    link: "/app/trash",
    icon: <DeleteOutlined />,
  },
  explore: {
    key: 4,
    name: "Explore",
    link: "/app/explore",
    icon: <CompassOutlined />,
  },
}

const Navigation = () => {
  const { capacity } = useContext(AuthContext)
  const { storageUsed } = useContext(StorageContext)

  const location = useLocation()
  const name = location.pathname.split("/")[2]

  return (
    <Menu
      mode="inline"
      className="nav-layout"
      selectedKeys={[MENUITEMS[name].key].toString()}
    >
      <div className="logo" style={{ background: "transparent" }}></div>
      {Object.keys(MENUITEMS).map(i => (
        <Menu.Item key={MENUITEMS[i].key} icon={MENUITEMS[i].icon}>
          <Link to={MENUITEMS[i].link}>{MENUITEMS[i].name}</Link>
        </Menu.Item>
      ))}
      <div className="logo" style={{ background: "transparent" }}>
        <div style={{ padding: "24px" }}>
          <div style={{ paddingBottom: "8px" }}>
            <small>
              <span role="img" aria-label="emoji">
                🗂️
              </span>
              Storage
            </small>
          </div>
          <Progress
            percent={(
              (storageUsed / 1024 / 1024 / Math.ceil(capacity / 1024 / 1024)) *
              100
            ).toFixed(1)}
            size="small"
            showInfo={false}
          />
          <div style={{ paddingTop: "8px" }}>
            <small>
              {(storageUsed / 1024 / 1024).toFixed(1)}/
              {Math.ceil(capacity / 1024 / 1024)} MB used
            </small>
          </div>
        </div>
      </div>
    </Menu>
  )
}

export default Navigation
