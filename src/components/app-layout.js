import React from "react"
import { Layout } from "antd"
import SideHeaderNav from "./side-header/index"
import "./app-layout.scss"

const { Content, Footer, Sider } = Layout

const AppLayout = ({ children, nav }) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
      >
        <SideHeaderNav />
      </Sider>
      <Sider
        className="site-layout-background"
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: "80px",
        }}
      >
        {nav}
      </Sider>
      <Layout className="site-layout-background" style={{ marginLeft: 280 }}>
        <Content style={{ margin: "16px", overflow: "initial" }}>
          <section className="section-layout">{children}</section>
        </Content>
        <Footer
          className="site-layout-background"
          style={{ textAlign: "center" }}
        >
          &copy; {new Date().getFullYear()} Gatsby + Firebase Firestore & Storage | Visit me <a href="https://ayunita.xyz/" target="_blank" rel="noopener noreferrer">here</a>
        </Footer>
      </Layout>
    </Layout>
  )
}

export default AppLayout
