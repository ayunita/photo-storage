import React, { useContext } from "react"
import { navigate } from "gatsby"
import { Button, Avatar, Menu } from "antd"
import { LogoutOutlined } from "@ant-design/icons"
import { logout } from "../../services/auth"
import { AuthContext } from "../../context/AuthProvider"

const SideHeaderNav = () => {
  const { name } = useContext(AuthContext)

  return (
    <>
      <Menu mode="inline" selectable={false} className="action-nav-layout">
      <div className="logo" style={{ background: "transparent" }}></div>
        <Menu.Item key="1">
          <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
            {name.charAt(0).toUpperCase()}
          </Avatar>
        </Menu.Item>
        {/* <Menu.Item key="2">
          <Button
            type="link"
            icon={
              <SettingOutlined style={{ paddingLeft: "8px", color: "white" }} />
            }
            onClick={e => {
             alert("Setting")
            }}
          />
        </Menu.Item> */}
        <Menu.Item key="3">
          <Button
            type="link"
            icon={
              <LogoutOutlined style={{ paddingLeft: "8px", color: "white" }} />
            }
            onClick={e => {
              e.preventDefault()
              logout(() => navigate(`/`))
            }}
          />
        </Menu.Item>
      </Menu>
    </>
  )
}
export default SideHeaderNav
