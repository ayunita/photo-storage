import React, { useState } from "react"
import { Alert, Form, Input, Button } from "antd"
import { navigate } from "gatsby"
import { handleLogin, isLoggedIn } from "../../services/auth"

const Login = () => {
  // if user login, then redirect to album
  if (isLoggedIn()) {
    navigate(`/app/album`)
  }

  const [error, setError] = useState(false)

  const onFinish = values => {
    setError(false)
    const auth = handleLogin(values)
    auth.then(res => {
      if (res) navigate(`/app/album`)
      else setError(true)
    })
  }

  const onFinishFailed = errorInfo => {
    setError(true)
  }

  return (
    <div className="login-form">
      <h1>Log in</h1>
      {error ? (
        <Alert
        style={{ margin: "16px 0"}}
          description="Incorrect username or password."
          type="error"
          showIcon
        />
      ) : null}
      <Form
        name="loginForm"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="email"
          hasFeedback
          rules={[{ type: "email", required: true, message: "" }]}
        >
          <Input placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: ""}]}
        >
          <Input.Password placeholder="Password" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Login
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login
