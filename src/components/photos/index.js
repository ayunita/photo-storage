import React, { useState, useEffect, useContext } from "react"
import { Link } from "gatsby"
import pathParse from "path-parse"
import {
  Breadcrumb,
  Card,
  Row,
  Col,
  Upload,
  message,
  Button,
  Progress,
} from "antd"
import { UploadOutlined } from "@ant-design/icons"
import Preview from "./preview"
import Info from "./info"
import * as AlbumService from "../../services/album"
import * as PhotoService from "../../services/photo"
import { AuthContext } from "../../context/AuthProvider"
import { StorageContext } from "../../context/StorageProvider"
const { Meta } = Card

const new_upload_default = {
  start: false,
  progress: 0,
  success: false,
  error: null,
  photo: "",
}

const Photos = props => {
  const { id, capacity } = useContext(AuthContext)
  const { albums, storageUsed } = useContext(StorageContext)
  const { folderId } = props

  const [album, setAlbum] = useState({})
  const [filteredList, setFilteredList] = useState([])
  const [photoList, setPhotoList] = useState([])
  const [lightbox, setLightbox] = useState({ photoIndex: 0, isOpen: false })
  const [drawer, setDrawer] = useState({ photoIndex: 0, isOpen: false })

  useEffect(() => {
    const album_data = albums.filter(i => i.id === folderId)
    setAlbum(album_data[0])
    const photos_data = album_data[0].photos
    const active = photos_data.filter(i => !i.removed)
    setFilteredList(active)
    setPhotoList(photos_data)
  }, [albums, folderId])

  const [upload, setUpload] = useState(new_upload_default)

  const beforeUploadPhoto = file => {
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error("Image must be smaller than 2MB!")
    }
    if (file.size > capacity - storageUsed) {
      message.error("You're running low on storage space.")
    }
    return isLt2M
  }

  const onUploadSnapshot = snapshot => {
    const progress = Math.round(
      (100 * snapshot.bytesTransferred) / snapshot.totalBytes
    )
    setUpload({
      ...upload,
      start: true,
      progress: progress,
    })
  }

  const onUploadError = error => {
    setUpload({
      ...upload,
      error: error,
      success: false,
    })
  }

  // const getPhotoDuplicate = uploadedName => {
  //   return photoList.filter(photo => photo.name === uploadedName).length
  // }

  const loadImage = url => {
    return new Promise(function (resolved, rejected) {
      var img = new Image()
      img.src = url
      img.onload = function () {
        resolved(img)
      }
    })
  }

  const onSuccessDownloadUrl = (url, file, path) => {
    const { ext, name } = pathParse(file.name)
    // const duplicate = getPhotoDuplicate(name)
    let photoName = name
    // if (duplicate > 0) photoName = `${name} (${duplicate})`

    loadImage(url).then(img => {
      const newPhoto = {
        created: new Date().toUTCString(),
        updated: new Date().toUTCString(),
        removed: false,
        name: photoName,
        ext: ext,
        src: url,
        path: path,
        size: file.size,
        width: img.naturalWidth,
        height: img.naturalHeight,
      }

      const photos = [...photoList, newPhoto]
      const albumSize = album.size + file.size

      const updateAlbumPhoto = AlbumService.updateAlbumField(
        id,
        folderId,
        "photos",
        photos
      )
      const updateAlbumSize = AlbumService.updateAlbumField(
        id,
        folderId,
        "size",
        albumSize
      )
      Promise.all([updateAlbumPhoto, updateAlbumSize])
        .then(res => {
          // reset upload status
          setUpload({
            start: false,
            progress: 0,
            success: true,
            error: null,
          })
          message.success("Photo was uploaded successfully.")
        })
        .catch(err => message.error("Network error."))
    })
  }

  return (
    <>
      <div style={{ padding: "8px" }}>
        <Breadcrumb style={{ paddingBottom: "16px" }}>
          <Breadcrumb.Item>
            <Link to="/app/album" replace>
              Albums
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>{album.title}</Breadcrumb.Item>
        </Breadcrumb>
        <Row gutter={[24, 24]}>
          <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 6 }}>
            <Card className="add-photo-button">
              <Upload
                accept="image/*"
                beforeUpload={beforeUploadPhoto}
                customRequest={data =>
                  PhotoService.uploadPhoto(
                    folderId,
                    data.file,
                    onUploadSnapshot,
                    onUploadError,
                    onSuccessDownloadUrl
                  )
                }
                showUploadList={false}
              >
                <Button icon={<UploadOutlined />}>Upload a photo</Button>
                {upload.start && (
                  <Progress type="line" percent={upload.progress} />
                )}
              </Upload>
            </Card>
          </Col>
          {filteredList.map((item, index) => {
            let component = null
            if (!item.removed)
              component = (
                <Col
                  key={index}
                  xs={{ span: 12 }}
                  sm={{ span: 8 }}
                  md={{ span: 6 }}
                >
                  <Card
                    className="photo-thumbnail"
                    cover={
                      <button
                        className="button-on-hide"
                        onClick={() =>
                          setLightbox({ photoIndex: index, isOpen: true })
                        }
                      >
                        <img alt={item.name} src={item.src} />
                      </button>
                    }
                  >
                    <Meta
                      style={{ textAlign: "center" }}
                      description={
                        <button
                          className="button-on-hide"
                          onClick={() =>
                            setDrawer({ photoIndex: index, isOpen: true })
                          }
                        >
                          {item.name}
                        </button>
                      }
                    />
                  </Card>
                </Col>
              )
            return component
          })}
        </Row>
      </div>
      {lightbox.isOpen && (
        <Preview
          images={filteredList}
          lightbox={lightbox}
          setLightbox={setLightbox}
        />
      )}
      {drawer.isOpen && (
        <Info
          id={id}
          folderId={folderId}
          photoList={filteredList}
          drawer={drawer}
          setDrawer={setDrawer}
        />
      )}
    </>
  )
}

export default Photos
