import React from "react"
import axios from "axios"
import fileDownload from "js-file-download"
import { Button, Drawer } from "antd"
import { DownloadOutlined } from "@ant-design/icons"

const DescriptionItem = ({ label, content }) => (
  <div className="photo-info-wrapper">
    <span className="photo-info-label">{label}</span>
    <span className="photo-info-content">{content}</span>
  </div>
)

const Info = ({ id, folderId, photoList, drawer, setDrawer }) => {
  const photo = photoList[drawer.photoIndex]

  const localeTime = date => {
    return new Date(date).toLocaleString("en-US", {
      month: "long",
      day: "2-digit",
      year: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    })
  }

  const downloadPhoto = (url, filename) => {
    axios
      .get(`https://cors-anywhere.herokuapp.com/${url}`, {
        responseType: "blob",
      })
      .then(res => {
        fileDownload(res.data, filename)
      })
  }

  return (
    <Drawer
      width="30%"
      placement="right"
      closable={false}
      onClose={() => setDrawer({ ...drawer, isOpen: false })}
      visible={drawer.isOpen}
    >
      <div className="photo-info-actions">
        <Button shape="circle" icon={<DownloadOutlined />} onClick={() => downloadPhoto(photo.src,`${photo.name}${photo.ext}`)} />
        {/* <Button shape="circle" icon={<DeleteOutlined />} onClick={() => markDelete()} /> */}
      </div>
      <div style={{ textAlign: "center", padding: "16px" }}>
        <img
          alt={photo.name}
          src={photo.src}
          style={{ width: "60%", height: "auto" }}
        />
      </div>
      <DescriptionItem label="Name" content={photo.name} />
      <DescriptionItem label="Size" content={`${photo.size} bytes`} />
      <DescriptionItem
        label="Type"
        content={`image/${photo.ext.substring(1)}`}
      />
      <DescriptionItem label="Created" content={localeTime(photo.created)} />
      <DescriptionItem label="Updated" content={localeTime(photo.updated)} />
    </Drawer>
  )
}
export default Info
