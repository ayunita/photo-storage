import React from "react"
import Lightbox from "react-image-lightbox"
import "react-image-lightbox/style.css"

const Preview = props => {
  const { images, lightbox, setLightbox, buttons } = props
  const main = images[lightbox.photoIndex].src
  const mainTitle =images[lightbox.photoIndex].name
  const next = images[(lightbox.photoIndex + 1) % images.length].src
  const prev = images[(lightbox.photoIndex + images.length - 1) % images.length].src

  return (
    <Lightbox
      mainSrc={main}
      imageTitle={mainTitle}
      nextSrc={next}
      prevSrc={prev}
      toolbarButtons={buttons}
      onCloseRequest={() => setLightbox({ ...lightbox, isOpen: false })}
      onMovePrevRequest={() =>
        setLightbox({
          ...lightbox,
          photoIndex: (lightbox.photoIndex + images.length - 1) % images.length,
        })
      }
      onMoveNextRequest={() =>
        setLightbox({
          ...lightbox,
          photoIndex: (lightbox.photoIndex + 1) % images.length,
        })
      }
    />
  )
}

export default Preview
