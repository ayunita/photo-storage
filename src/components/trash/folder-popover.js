import React, { useState } from "react"
import { Popover, Button, message } from "antd"
import * as AlbumService from "../../services/album"

const FolderPopover = ({ children, userId, folder }) => {
  const [visible, setVisible] = useState(false)

  const restoreAlbum = () => {
    AlbumService.updateAlbumField(userId, folder.id, "removed", false)
      .then(res => {
        setVisible(false)
        message.success("Album was restored.")
      })
      .catch(err => message.error("Network error."))
  }

  return (
    <Popover
      placement="right"
      trigger="click"
      visible={visible}
      onVisibleChange={setVisible}
      content={
        <div className="folder-actions">
          <Button
            icon={
              <span role="img" aria-label="emoji-restore">
               📒
              </span>
            }
            onClick={() => restoreAlbum()}
          >
            Restore
          </Button>
        </div>
      }
    >
      {children}
    </Popover>
  )
}

export default FolderPopover
