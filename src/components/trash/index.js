import React, { useState, useContext } from "react"
import { AuthContext } from "../../context/AuthProvider"
import { StorageContext } from "../../context/StorageProvider"
import { COLOR_LIST } from "../../data/form-data"
import { Button, Breadcrumb, Table, Empty } from "antd"
import { FolderFilled } from "@ant-design/icons"
import * as AlbumService from "../../services/album"
import * as PhotoService from "../../services/photo"

const columns = [
  {
    title: "Title",
    dataIndex: "title",
    render: (title, row) => (
      <div style={{ display: "flex", alignItems: "center" }}>
        <FolderFilled
          className="album-thumbnail-icon"
          style={{
            marginRight: "8px",
            fontSize: "1.5rem",
            color: COLOR_LIST[row.color],
          }}
        />
        {title}
      </div>
    ),
  },
  {
    title: "Size",
    dataIndex: "size",
    render: size => <span>{(size / 1024 / 1024).toFixed(1)} MB</span>,
  },
  {
    title: "Modified",
    dataIndex: "updated",
    render: updated => (
      <span>
        {new Date(updated).toLocaleString("en-US", {
          month: "long",
          day: "2-digit",
          year: "numeric",
          hour: "2-digit",
          minute: "2-digit",
        })}
      </span>
    ),
  },
]

const Trash = () => {
  const { id } = useContext(AuthContext)
  const { albums } = useContext(StorageContext)
  const [removedAlbums, setRemovedAlbums] = useState(
    albums.filter(a => a.removed)
  )

  const deleteAlbums = () => {
    removedAlbums.forEach(a => {
      const p1 = PhotoService.deleteAllPhotos(a.id)
      const p2 = AlbumService.deleteAlbum(id, a.id)
      Promise.all([p1, p2])
    })
    setRemovedAlbums([])
  }

  return (
    <>
      <div style={{ padding: "8px" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Breadcrumb style={{ paddingBottom: "16px" }}>
            <Breadcrumb.Item>Trash Bin</Breadcrumb.Item>
          </Breadcrumb>
          <Button
            type="primary"
            size="small"
            onClick={() => deleteAlbums()}
          >
            Empty trash
          </Button>
        </div>
        {removedAlbums.length > 0 ? (
          <Table
            columns={columns}
            dataSource={removedAlbums.map(a => ({ ...a, key: a.title }))}
          />
        ) : (
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description={
                <span>What a clean trash bin</span>
            }
          />
        )}
      </div>
    </>
  )
}

export default Trash
