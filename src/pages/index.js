import React from "react"
import Login from "../components/login"

const IndexPage = () => (
  <div style={{
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: "64px"
  }}>
    <h2>Gatsby photo storage</h2>
    <Login />
    <div style={{ padding: "32px 0" }}>Test account: test@test.com $FWQo12#</div>
  </div>
)

export default IndexPage
