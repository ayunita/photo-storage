import React from "react"
import { Router } from "@reach/router"
import AppLayout from "../components/app-layout"
import Navigation from "../components/navigation"
import Album from "../components/album/index"
import Favorites from "../components/favorites/index"
import Trash from "../components/trash/index"
import Explore from "../components/explore/index"
import Photos from "../components/photos/index"
import PrivateRoute from "../components/private-route"
import AuthProvider from "../context/AuthProvider"
import StorageProvider from "../context/StorageProvider"

const AppPage = () => {
  return (
    <AuthProvider>
      <StorageProvider>
        <AppLayout nav={<Navigation />}>
          <Router basepath="/app">
            <PrivateRoute path="/album" component={Album} />
            <PrivateRoute path="/fav" component={Favorites} />
            <PrivateRoute path="/album/:folderId" component={Photos} />
            <PrivateRoute path="/trash" component={Trash} />
            <PrivateRoute path="/explore" component={Explore} />
          </Router>
        </AppLayout>
      </StorageProvider>
    </AuthProvider>
  )
}

export default AppPage
