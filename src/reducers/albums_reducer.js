import * as ACTION_TYPES from "../actions/actions_types"

const initialState = {}
export const AlbumsReducer = (state = initialState, action) => {
  const payload = action.payload

  switch (action.type) {
    case ACTION_TYPES.SET_ALBUMS:
      return {
        ...payload,
      }
    case ACTION_TYPES.UPDATE_ALBUM_PHOTOS:
      const edit_index = Object.keys(state).find(i => state[i].id === payload.album_id)
      const newState = {...state}
      newState[edit_index].photos = payload.photos
      newState[edit_index].size = payload.storage_used
      return { ...newState }
    default:
      return state
  }
}
