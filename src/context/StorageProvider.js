import React, { useEffect, useState, useContext } from "react"
import { AuthContext } from "../context/AuthProvider"
import * as AlbumService from "../services/album"

export const StorageContext = React.createContext({})

const StorageProvider = ({ children }) => {
  const { id } = useContext(AuthContext)
  //   const [albums, dispatchAlbums] = useReducer(AlbumsReducer, {})
  const [component, setComponent] = useState(null)

  useEffect(() => {
    let unmounted = false
    AlbumService.albumRef(id)
      .orderBy("title", "asc")
      .onSnapshot(snapshot => {
        let storageUsed = 0
        const data = snapshot.docs.map(doc => {
          const album = doc.data()
          const used = album.photos.reduce((acc, a) => acc + a.size, 0)
          storageUsed += used
          return {
            id: doc.id,
            ...album,
          }
        })
        if (!unmounted) {
          setComponent(
            <StorageContext.Provider value={{ albums: data, storageUsed: storageUsed}}>
              {children}
            </StorageContext.Provider>
          )
        }
      })
    return () => {
      unmounted = true
    }
  }, [id, children])

  return component
}

export default StorageProvider
