import React from "react"
import { getUser } from "../services/auth"

export const AuthContext = React.createContext({})

const AuthProvider = ({children}) => (
  <AuthContext.Provider
    value={getUser()}
  >
    {children}
  </AuthContext.Provider>
)

export default AuthProvider