import firebase from "gatsby-plugin-firebase"
import "firebase/firestore"

const db = firebase.firestore()

const usersRef = db.collection("users")

const auth = (email, password) => {
  return usersRef
    .where("email", "==", email)
    .where("password", "==", password)
    .get()
}

export const isBrowser = () => typeof window !== "undefined"

export const getUser = () =>
  isBrowser() && window.localStorage.getItem("testUser")
    ? JSON.parse(window.localStorage.getItem("testUser"))
    : {}

const setUser = user =>
  window.localStorage.setItem("testUser", JSON.stringify(user))

export const handleLogin = async ({ email, password }) => {
  try {
    const snapshot = await auth(email, password)
    if (snapshot.size > 0) {
      const data = snapshot.docs[0]
      setUser({
        id: data.id,
        ...data.data(),
      })
      return true
    } else return false
  } catch (error) {
    return false
  }
}
export const isLoggedIn = () => {
  const user = getUser()
  return !!user.email
}

export const logout = callback => {
  setUser({})
  callback()
}
