import firebase from "gatsby-plugin-firebase"
import "firebase/firestore"

const db = firebase.firestore()

export const usersRef = db.collection("users")

export const getAllUsers = () => {
  return usersRef.get()
}

export const getUser = (email, password) => {
  return usersRef.where("email", "==", email).where("password", "==", password).get()
}