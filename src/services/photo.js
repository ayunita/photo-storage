import firebase from "gatsby-plugin-firebase"
import "firebase/storage"

const storageRef = firebase.storage()

export const uploadPhoto = (
  folderId,
  file,
  onSnapshot,
  onError,
  onDownloadUrl
) => {
  const fileRef = storageRef.ref(`${folderId}/${file.uid}`)
  const fileUpload = fileRef.put(file)
  fileUpload.on(
    firebase.storage.TaskEvent.STATE_CHANGED,
    onSnapshot,
    onError,
    () => {
      storageRef
        .ref(folderId)
        .child(file.uid)
        .getDownloadURL()
        .then(url => onDownloadUrl(url, file, fileRef.fullPath))
    }
  )
}

export const deletePhoto = (path) => {
  return storageRef.ref(path).delete()
}

export const deleteAllPhotos = (path) => {
  storageRef.ref(path).listAll().then((listResults) => {
    const promises = listResults.items.map((item) => {
      return item.delete();
    });
    Promise.all(promises);
  });
}