import firebase from "gatsby-plugin-firebase"
import "firebase/firestore"

const db = firebase.firestore()

export const albumRef = userId => {
  return db.collection("users").doc(userId).collection("albums")
}

export const contentRef = (userId, folderId) => {
  return albumRef(userId).doc(folderId)
}

export const getTotalAlbumSize = async userId => {
  let size = 0
  const calculate = () => albumRef(userId).where("size", ">", 0).get()

  const snapshot = await calculate()
  if (snapshot.size > 0) {
    snapshot.forEach(doc => (size += doc.data().size))
  }

  return size
}

export const getAllSharedAlbums = userId => {
  return albumRef(userId).where("isPrivate", "==", false).get()
}

export const addAlbum = (userId, album) => {
  return albumRef(userId).add({
    // created: firebase.firestore.FieldValue.serverTimestamp(),
    ...album,
  })
}

export const updateAlbumField = (userId, folderId, key, val) => {
  return albumRef(userId)
    .doc(folderId)
    .update({
      [key]: val,
    })
}

export const updateAlbum = (userId, folderId, album) => {
  return albumRef(userId)
    .doc(folderId)
    .update({
      ...album,
    })
}

export const deleteAlbum = (userId, folderId) => {
  return contentRef(userId, folderId).delete()
}
