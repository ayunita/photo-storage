import * as ACTION_TYPES from "./actions_types"

export const SET_ALBUMS = (albums) => {
    return {
        type: ACTION_TYPES.SET_ALBUMS,
        payload: albums
    }
}

export const UPDATE_ALBUM_PHOTOS = (album_id, storage_used, photos) => {
    return {
        type: ACTION_TYPES.UPDATE_ALBUM_PHOTOS,
        payload: {
            album_id: album_id,
            storage_used: storage_used,
            photos: photos,
        }
    }
}